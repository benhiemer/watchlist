<?php

interface Finder {

    /**
     * Find information about a movie based on its id.
     */
    public function findMovie($id);
}

class FinderFactory {
    public static function create($apiKey) {
        return new MovieDB($apiKey);
    }
}
