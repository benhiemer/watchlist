<?php

/**
 * Implementation of Finder. Collects movie information using
 * "https://themoviedb.org"
 */
class MovieDB implements Finder {

    const MOVIEDB_URL_BASE = "https://api.themoviedb.org/3/movie/";
    const MOVIEDB_URL_IMAGE_400 = "https://image.tmdb.org/t/p/w400";
    const VIDEOS_URL_YOUTUBE = "https://www.youtube.com/watch?v=";

    private $apiKey;
    
    public function __construct($apiKey) {
        $this->apiKey = $apiKey;
    }

    public function findMovie($id) {
        
        if ($this->apiKey == "insert-your-api-key-here") {
            throw new Exception('API key is not valid');
        }

       // use themoviedb to get movie information
       $url = self::MOVIEDB_URL_BASE . $id . "?api_key=" . $this->apiKey . "&language=en-US&append_to_response=credits,videos";
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt($ch, CURLOPT_URL, $url);
       $result = curl_exec($ch);
       curl_close($ch);

       $movie_data = [];

       if (isset($result)) {
           // parse json
           $movie_data = $this->parseResultFromMovieDB($result, $id);
       }
       return $movie_data;
    }

    private function collect($items) {
      $result = array();
      foreach ($items as $item) {
        $result[] = $item["name"];
      }
      return implode(", ", $result);
    }

    private function parseResultFromMovieDB($movie, $id) {
        $json_a = json_decode($movie, true);

        $movie_data = [];
        $movie_data['id'] = $id;
        $movie_data['title'] = $json_a['original_title'] ?? '';
        $movie_data['plot'] = $json_a['overview'] ?? '';
        $movie_data['runtime'] = $json_a['runtime'] ?? '';
        $movie_data['release'] = $json_a['release_date'] ?? '';
        if (isset($json_a['poster_path'])) {
            $movie_data['posterURL'] = self::MOVIEDB_URL_IMAGE_400 . $json_a['poster_path'];
        } else {
            $movie_data['posterURL'] = '';
        }

        $countries = $json_a['production_countries'] ?? [];
        $movie_data['countries'] = $this->collect($countries);

        $genres = $json_a['genres'] ?? [];
        $movie_data['genres'] = $this->collect($genres);

        $cast_arr = $json_a['credits']['cast'] ?? [];
        $i = count($cast_arr);

        if ($i > 4) {
            $i = 4;
        }

        $stars = array();
        foreach ($cast_arr as $item) {
            if ($i <= 0) {
                break;
            }
            $stars[] = $item['name'];

            $i--;
        }
        $str_stars = implode(", ", $stars);
        $movie_data['stars'] = $str_stars;

        $crew_arr = $json_a['credits']['crew'] ?? [];
        $directors = array();
        foreach ($crew_arr as $item) {
            if ($item['job'] === 'Director') {
                $directors[] = $item["name"];
            }
        }
        $str_director = implode(", ", $directors);
        $movie_data['directors'] = $str_director;
          
        $videos_arr = $json_a['videos']['results'] ?? [];
        $video = '';
        foreach ($videos_arr as $item) {
            if ($item['type'] === 'Trailer' && $item['site'] === 'YouTube') {
                $video = self::VIDEOS_URL_YOUTUBE . $item['key'];
                break;
            }
        }
        $movie_data['video'] = $video;
          
        return $movie_data;
    }
}
