<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => true,
        'db' => [
            'type' => 'json'
        ],
        'finder' => [
            'name' => 'moviedb',
            'apiKey' => 'insert-your-api-key-here'
        ]
    ]
];
