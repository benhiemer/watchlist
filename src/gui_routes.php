<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response) {
    $this->logger->debug("Receiving GET request...");
    $api = new Api($this->finder, $this->database, $this->logger);
    $result = $api->getMovies();

    if ($result['status']['code'] == 200) {
        
        $movieAddUrl = $this->router->pathFor('movie_add');
        $movieDeleteUrl = $this->router->pathFor('movie_delete');
        $movieReloadUrl = $this->router->pathFor('movie_reload');
        return $this->view->render($response, "movies.twig", ['movies' => array_reverse($result['data']), 'urls' => array('movie_add' => $movieAddUrl, 'movie_delete' => $movieDeleteUrl, 'movie_reload' => $movieReloadUrl)]);
    } else {
        return $this->view->render($response, "error.twig", ['status' => $result['status'], 'urls' => array('home' => $this->router->pathFor('home'))]);
    }
})->setName('home');

$app->post('/movies/reload', function (Request $request, Response $response) {
    $this->logger->debug("Receiving POST request...");
    $api = new Api($this->finder, $this->database, $this->logger);
    $result = $api->getMovies();
    
    if ($result['status']['code'] == 200) {
        $movies = $result['data'];
        
        foreach ($movies as $movie) { 
            $id = $movie['id'];
            $api->deleteMovie($id);
            $api->addMovie($id);
        } 
        $url = $this->router->pathFor('home');
        return $response->withStatus(303)->withHeader('Location', $url);
    } else {
        return $this->view->render($response, "error.twig", ['status' => $result['status'], 'urls' => array('home' => $this->router->pathFor('home'))]);
    }  
})->setName('movie_reload');

$app->post('/movies/add', function (Request $request, Response $response) {
    $this->logger->debug("Receiving POST request...");
    $data = $request->getParsedBody();
    $id = filter_var($data['imdbID'], FILTER_SANITIZE_STRING);

    $api = new Api($this->finder, $this->database, $this->logger);
    $result = $api->addMovie($id);

    if ($result['status']['code'] == 200) {

        $url = $this->router->pathFor('home');
        return $response->withStatus(303)->withHeader('Location', $url);
    } else {
        return $this->view->render($response, "error.twig", ['status' => $result['status'], 'urls' => array('home' => $this->router->pathFor('home'))]);
    }
})->setName('movie_add');

$app->post('/movies/delete', function (Request $request, Response $response) {
    $this->logger->debug("Receiving DELETE request...");
    $data = $request->getParsedBody();
    $id = filter_var($data['imdbID'], FILTER_SANITIZE_STRING);

    $api = new Api($this->finder, $this->database, $this->logger);
    $result = $api->deleteMovie($id);

    if ($result['status']['code'] == 204) {

        $url = $this->router->pathFor('home');
        return $response->withStatus(303)->withHeader('Location', $url);
    } else {
        return $this->view->render($response, "error.twig", ['status' => $result['status'], 'urls' => array('home' => $this->router->pathFor('home'))]);
    }
})->setName('movie_delete');
