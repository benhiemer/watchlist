<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Delete a single movie from the database identified by its id.
 */
$app->delete('/api/movies/{id}', function (Request $request, Response $response) {
    $this->logger->debug("Receiving DELETE request...");
    $id = $request->getAttribute('id');

    $api = new Api($this->finder, $this->database, $this->logger);
    $result = $api->deleteMovie($id);
    return $response->withJson(json_encode($result));
});

/**
 * Get information about a single movie identified by its id.
 */
$app->get('/api/movies/{id}', function (Request $request, Response $response) {
    $this->logger->debug("Receiving GET request with id...");
    $id = $request->getAttribute('id');
    
    $api = new Api($this->finder, $this->database, $this->logger);
    $result = $api->getMovie($id);
    return $response->withJson(json_encode($result));
});

/**
 * Get information about all movies in the database.
 */
$app->get('/api/movies', function (Request $request, Response $response) {
    $this->logger->debug("Receiving GET request...");
    
    $api = new Api($this->finder, $this->database, $this->logger);
    $result = $api->getMovies();
    return $response->withJson(json_encode($result));
});

/**
 * Add a new movie to the database.
 */
$app->post('/api/movies', function (Request $request, Response $response) {
    $this->logger->debug("Receiving POST request...");
    $data = $request->getParsedBody();
    $id = filter_var($data['imdbID'], FILTER_SANITIZE_STRING);

    $api = new Api($this->finder, $this->database, $this->logger);
    $result = $api->addMovie($id);
    return $response->withJson(json_encode($result));
});
