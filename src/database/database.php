<?php

interface Database {

    public function init();
    
    public function insert($movie_data);
    
    public function delete($id);
    
    public function get($id);
    
    public function getAll();
}
