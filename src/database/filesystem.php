<?php

/**
 *   
 */
class FileSystem implements Database {

    private $dir;
    private $logger;
    
    public function __construct($dir, $logger) {
        $this->dir = $dir;
        $this->logger = $logger;
    }
    
    public function init() {
        
        if (is_dir($this->dir)) {
            return;
        }
        
        if (is_file($this->dir)) {
            unlink($this->dir);
        }
        
        mkdir($this->dir, 0777, true);
        $this->logger->debug("Successfully created directory to use JSON files as database");
    }
    
    public function insert($movie_data) {
        $id = $movie_data["id"];
        $file = $this->getFile($id);  
        file_put_contents($file, json_encode($movie_data));
        $this->logger->debug("Successfully created JSON file for id", ['data' => $id]);
    }
    
    public function delete($id) {
        $file = $this->getFile($id);
        if (!file_exists($file)) {
            return;
        }  
        unlink($file);      
        $this->logger->debug("Successfully deleted JSON file for id", ['data' => $id]);
    }
    
    public function get($id) {
        $file = $this->getFile($id);
        if (!file_exists($file)) {
            return json_decode("{}", true);
        }
        $string = file_get_contents($file);
        return json_decode($string, true);
    }
    
    public function getAll() {
        $result = array();
        $filePattern = $this->dir . "/*.json";
        $fileList = glob($filePattern);
        foreach ($fileList as $filename) {
            if (is_file($filename)) {
                $string = file_get_contents($filename);
                $json = json_decode($string, true);
                $result[filemtime($filename)] = $json; 
            }   
        }
        ksort($result);   
        return array_values($result);           
    }
    
    private function getFile($id) {
        return $this->dir . $id . '.json';
    }
}
