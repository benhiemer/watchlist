            <footer>
                <p class="float-right"><a href="#">Back to top</a></p>
                <p>
                    &copy; 2019 Benedikt Hiemenz.
                    &middot; This product uses the TMDb API but is not endorsed or certified by TMDb.
                </p>
            </footer>

        </div>
    </body>
</html>
