# watchlist

## Setup
1. Install php 7
2. Download composer.phar
3. cd watchlist
4. php ../path/to/composer.phar install
5. Configure application using /src/settings.php
6. php -S localhost:8080 src/index.php
7. Open browser on http://localhost:8080 

or

1. docker-compose up

## TODO
* Store images locally (cache images)
* Add MongoDB support
